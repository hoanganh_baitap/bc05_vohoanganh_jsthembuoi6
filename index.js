document.getElementById("btnSoNguyenTo").onclick = function () {
  console.log("yes");
  //   input: number
  var number = document.getElementById("txt-num").value * 1;
  //   console.log('number: ', number);
  // output: string
  var soNguyenTo = "";
  // xử lý
  for (var i = 0; i <= number; i++) {
    if (laSoNguyenTo(i) == true) {
      soNguyenTo += i + "  ";
    }
  }
  // show kết quả
  document.getElementById("result").innerText = soNguyenTo;
};

function laSoNguyenTo(n) {
  if (n < 2) {
    return false;
  }
  if (n == 2) {
    return true;
  }
  if (n % 2 == 0) {
    return false;
  }
  for (var i = 3; i < n - 1; i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}
